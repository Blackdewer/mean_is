//Dependencias
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');

const config = require('./config/database');
const users = require('./routes/users');
const students = require('./routes/students');
const representatives = require('./routes/representatives');

//Conexión con la base de datos
mongoose.connect(config.database);

//Chequear si se logró conectar con la base de datos
mongoose.connection.on('connected', function() {
	console.log('Connected to database: ' + config.database);
});

//Chequear si ocurrió algun error al conectar con la base de datos
mongoose.connection.on('error', function(err) {
	console.log('Database error: ' + err);
});

//Inicialización de la variable "app" con el modulo Express
const app = express();

//Variable del puerto a utilizar
const port = 3000;

//CORS Middleware
app.use(cors());

//Información de la carpeta "public"
app.use(express.static(path.join(__dirname, 'public')));

//Body-Parser Middleware
app.use(bodyParser.json());

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

//Middleware para utilizar las rutas de usuario
app.use('/users', users);

//Middleware para utilizar las rutas de estudiante
app.use('/students', students);

//Middleware para utilizar las rutas de representante
app.use('/representatives', representatives);

//GET request a la página de inicio
app.get('/', function(req, res) {
	res.send('Invalid Endpoint');
});

//Inicialización del Servidor
app.listen(port, function() {
	console.log('Server started on port ' + port + '...');
});