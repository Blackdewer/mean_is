//Dependencias
const express = require('express');
const router = express.Router();

const config = require('../config/database');
const Representative = require('../models/representative');
const Student = require('../models/student');

//Rutas

//Página de Inserción
router.post('/insert', function(req, res, next) {
	const ci = req.body.ci;
	Representative.getRepresentativeByCI(ci, function(err, representative) {
		if(err) throw err;
		if(!representative) {
			let newRepresentative = new Representative({
				//Atributos del representante
				ci: req.body.ci,
				name: req.body.name,
				lastname: req.body.lastname,
				civil_status: (req.body.civil_status == undefined ? null : req.body.civil_status),
				address: req.body.address,
				cellphone: (req.body.cellphone == undefined ? null : req.body.cellphone),
				telephone: (req.body.telephone == undefined ? null : req.body.telephone),
				email: (req.body.email == undefined ? null : req.body.email),
				occupation: (req.body.occupation == undefined ? null : req.body.occupation),
				company: (req.body.company == undefined ? null : req.body.company),
				avg_income: (req.body.avg_income == undefined ? null : req.body.avg_income),
				house_type: (req.body.house_type == undefined ? null : req.body.house_type),
				house_status: (req.body.house_status == undefined ? null : req.body.house_status)
			});
			Representative.addRepresentative(newRepresentative, function(err, representative) {
				if(err) {
					res.json({success: false, msg: 'Fallo de inserción de representante'});
				}
				else {
					res.json({success: true, msg: 'La información del representante fué insertada exitosamente'});
				}
			});
		}
		else {
			return res.json({success: false, msg: 'Fallo de inserción de representante: La cédula del representante ya se encuentra en la base de datos'});
		}
	});
});

//Busqueda de representante para luego editar/eliminar
router.post('/search', function(req, res, next) {
	const ci = req.body.r_ci;
	Representative.getRepresentativeByCI(ci, function(err, representative) {
		if(err) throw err;
		if(!representative) {
			return res.json({success: false, msg: 'Representante no encontrado'});
		}
		else {
			return res.json({success: true, representative: representative});
		}
	});
});

//Edición de representante ya existente
router.post('/edit-rep/:id', function(req, res, next) {
	let representative = {};
	//Atributos del representante
	representative.ci = req.body.representative.ci;
	representative.name = req.body.representative.name;
	representative.lastname = req.body.representative.lastname;
	representative.civil_status = req.body.representative.civil_status;
	representative.address = req.body.representative.address;
	representative.municipality = req.body.representative.municipality;
	representative.cellphone = req.body.representative.cellphone;
	representative.telephone = req.body.representative.telephone;
	representative.email = req.body.representative.email;
	representative.occupation = req.body.representative.occupation;
	representative.company = req.body.representative.company;
	representative.avg_income = req.body.representative.avg_income;
	representative.house_type = req.body.representative.house_type;
	representative.house_status = req.body.representative.house_status;
	//Ultima modificacion
	representative.last_modified = {
		date: Date.now(),
		by_user: req.body.user.username
	};
	/////////////////////
	let query = {_id:req.params.id}
	Representative.update(query, representative, function(err) {
		if(err) {
			return res.json({success: false, msg: 'Fallo de edición de representante'});
		}
		else {
			return res.json({success: true, msg: 'La edición del representante fué realizada exitosamente'});
		}
	});
});

//Eliminación de representante ya existente
router.post('/delete-rep/:id', function(req, res, next) {
	const r_ci = req.body.ci;
	Student.getStudentByRCI(r_ci, function(err, students) {
		if(err) throw err;
		if(!students || students == "") {
			let query = {_id:req.params.id}
			Representative.remove(query, function(err) {
				if(err) {
					return res.json({success: false, msg: 'Fallo de eliminación de representante'});
				}
				else {
					return res.json({success: true, msg: 'La eliminación del representante fué realizada exitosamente'});
				}
			});
		}
		else {
			let aux;
			students.forEach(student => {
				aux = ", " + student.ci;
			});
			return res.json({success: false, msg: "Fallo de eliminación de representante: Los siguientes estudiantes" + aux + "." + " Se encuentran vinculados con este representante."});
		}
	});
});

//Exportación del modulo "router"
module.exports = router;