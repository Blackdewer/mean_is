//Dependencias
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const config = require('../config/database');
const User = require('../models/user');

//Rutas

//Página de Registro
router.post('/register', function(req, res, next) {
	const username = req.body.username;
	User.getUserByUsername(username, function(err, user) {
		if(err) throw err;
		if(!user) {
			let newUser = new User({
				username: req.body.username,
				role: "Usuario",
				password: req.body.password
			});
			User.addUser(newUser, function(err, user) {
				if(err) {
					res.json({success: false, msg: 'Fallo de registro de usuario'});
				}
				else {
					res.json({success: true, msg: 'Usuario registrado exitosamente'});
				}
			});
		}
		else {
			return res.json({success: false, msg: 'Fallo de registro de usuario: El nombre de usuario ya se encuentra en la base de datos'});
		}
	});
});

//Autenticación de usuario
router.post('/authenticate', function(req, res, next) {
	const username = req.body.username;
	const password = req.body.password;
	User.getUserByUsername(username, function(err, user) {
		if(err) throw err;
		if(!user) {
			return res.json({success: false, msg: 'Usuario no encontrado'});
		}
		User.comparePassword(password, user.password, function(err, isMatch) {
			if(err) throw err;
			if(isMatch) {
				const token = jwt.sign({data:user}, config.secret, {
					expiresIn: 604800 // 1 week
				});
				res.json({
					success: true,
					token: 'JWT ' + token,
					user: {
						id: user._id,
						username: user.username,
						role: user.role
					}
				});
			}
			else {
				return res.json({success: false, msg: 'Contraseña incorrecta'});
			}
		});
	});
});

//Exportación del modulo "router"
module.exports = router;