//Dependencias
const express = require('express');
const router = express.Router();

const config = require('../config/database');
const Student = require('../models/student');

//Rutas

//Página de Inserción
router.post('/insert', function(req, res, next) {
	const ci = req.body.ci;
	Student.getStudentByCI(ci, function(err, student) {
		if(err) throw err;
		if(!student) {
			let newStudent = new Student({
				//Atributos del estudiante
				ci: req.body.ci,
				grade: (req.body.grade == undefined ? null : req.body.grade),
				section: (req.body.section == undefined ? null : req.body.section),
				name: req.body.name,
				lastname: req.body.lastname,
				country: req.body.country,
				state: req.body.state,
				municipality: req.body.municipality,
				cellphone: (req.body.cellphone == undefined ? null : req.body.cellphone),
				email: (req.body.email == undefined ? null : req.body.email),
				hobby: (req.body.hobby == undefined ? null : req.body.hobby),
				birthdate: req.body.birthdate,
				gender: req.body.gender,
				scholarship: req.body.scholarship,
				left_handed: req.body.left_handed,
				canaima: req.body.canaima,
				height: (req.body.height == undefined ? null : req.body.height),
				weight: (req.body.weight == undefined ? null : req.body.weight),
				shirt_size: (req.body.shirt_size == undefined ? null : req.body.shirt_size),
				pants_size: (req.body.pants_size == undefined ? null : req.body.pants_size),
				shoe_size: (req.body.shoe_size == undefined ? null : req.body.shoe_size),
				//Atributos del representante
				representative: {
					r_ci: req.body.representative.r_ci,
					kinship: (req.body.representative.kinship == undefined ? null : req.body.representative.kinship)
				}
			});
			Student.addStudent(newStudent, function(err, student) {
				if(err) {
					res.json({success: false, msg: 'Fallo de inserción de estudiante'});
				}
				else {
					res.json({success: true, msg: 'La información del estudiante fué insertada exitosamente'});
				}
			});
		}
		else {
			return res.json({success: false, msg: 'Fallo de inserción de estudiante: La cédula del estudiante ya se encuentra en la base de datos'});
		}
	});
});

//Busqueda de estudiante para luego editar/eliminar
router.post('/search', function(req, res, next) {
	const ci = req.body.ci;
	Student.getStudentByCI(ci, function(err, student) {
		if(err) throw err;
		if(!student) {
			return res.json({success: false, msg: 'Estudiante no encontrado'});
		}
		else {
			return res.json({success: true, student: student});
		}
	});
});

//Edición de estudiante ya existente
router.post('/edit/:id', function(req, res, next) {
	let student = {};
	//Atributos del estudiante
	student.ci = req.body.student.ci;
	student.name = req.body.student.name;
	student.lastname = req.body.student.lastname;
	student.country = req.body.student.country;
	student.state = req.body.student.state;
	student.municipality = req.body.student.municipality;
	student.cellphone = req.body.student.cellphone;
	student.email = req.body.student.email;
	student.hobby = req.body.student.hobby;
	student.birthdate = req.body.student.birthdate;
	student.gender = req.body.student.gender;
	student.scholarship = req.body.student.scholarship;
	student.left_handed = req.body.student.left_handed;
	student.canaima = req.body.student.canaima;
	student.height = req.body.student.height;
	student.weight = req.body.student.weight;
	student.shirt_size = req.body.student.shirt_size;
	student.pants_size = req.body.student.pants_size;
	student.shoe_size = req.body.student.shoe_size;
	//Atributos del representante
	student.representative = req.body.student.representative;
	//Ultima modificacion
	student.last_modified = {
		date: Date.now(),
		by_user: req.body.user.username
	};
	/////////////////////////////
	let query = {_id:req.params.id}
	Student.update(query, student, function(err) {
		if(err) {
			return res.json({success: false, msg: 'Fallo de edición de estudiante'});
		}
		else {
			return res.json({success: true, msg: 'La edición del estudiante fué realizada exitosamente'});
		}
	});
});

//Inscripcion/Modificación de estudiante ya existente
router.post('/enroll/:id', function(req, res, next) {
	const id = req.params.id;
	Student.getStudentById(id, function(err, student) {
		if(err) throw err;
		if(student) {
			//Atributos del estudiante
			student.grade = (req.body.grade == undefined ? null : req.body.grade);
			student.section = (req.body.section == undefined ? null : req.body.section);
			//Log de modificacion
			let aux = {	
				grade: (req.body.grade == undefined ? null : req.body.grade),
				section: (req.body.section == undefined ? null : req.body.section),
				commit: req.body.commit,
				date: Date.now()
			};
			student.enroll_log.push(aux);
			////////////////////////////
			let query = {_id:req.params.id}
			Student.update(query, student, function(err) {
				if(err) {
					return res.json({success: false, msg: 'Fallo de modificación de estudiante'});
				}
				else {
					return res.json({success: true, msg: 'La modificación del estudiante fué realizada exitosamente'});
				}
			});
		}
		else {
			return res.json({success: false, msg: 'Fallo de modificación de estudiante'});
		}
	});
});

//Eliminación de estudiante ya existente
router.delete('/delete/:id', function(req, res, next) {
	let query = {_id:req.params.id}
	Student.remove(query, function(err) {
		if(err) {
			return res.json({success: false, msg: 'Fallo de eliminación de estudiante'});
		}
		else {
			return res.json({success: true, msg: 'La eliminación del estudiante fué realizada exitosamente'});
		}
	});
});

//Exportación de datos de estudiante
router.get('/export1', function(req, res, next) {
	Student.find(function(err, students) {
		if(err) throw err;
		if(!students || students == "") {
			return res.json({success: false, msg: 'Fallo en la exportación de los datos'});
		}
		else {
			let count = 0;
			let data = [];
			students.forEach(student => {
				count = count + 1;
				let aux = {
					N: count,
					Cedula: student.ci,
					Apellidos: student.lastname,
					Nombres: student.name,
					Grado: student.grade,
					Seccion: student.section,
					Fecha_Naci: student.birthdate,
					Lugar_Naci: student.country,
					Estad_Naci: student.state,
					CedulaRepr: student.representative.r_ci,
					Parentesco: student.representative.kinship
				};
				data.push(aux);
			});
			console.log(data);
			return res.json({success: true, msg: "Datos exportados satisfactoriamente", students: data});
		}
	});
});

//Exportación del modulo "router"
module.exports = router;