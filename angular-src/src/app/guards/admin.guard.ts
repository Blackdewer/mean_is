import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AdminGuard implements CanActivate{
	constructor(private authService:AuthService, private router:Router) {}
	
	//Función utilizada para proteger rutas si no hay autenticación de administrador
	canActivate() {
		if(this.authService.adminLoggedIn()) {
			return true;
		}
		else {
			this.router.navigate(['/']);
			return false;
		}
	}
}