import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CsvService } from 'angular2-json2csv';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

	constructor(
		private _csvService: CsvService,
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService
	) { }

	ngOnInit() {
	}
	
	//Exportar Datos
	exportSubmit() {
		//Conexión con el backend para recibir datos de exportación
		this.authService.exportData1().subscribe(data => {
			if(data.success) {
				this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
				this._csvService.download(data.students, 'datosEstudiantes');
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
			}
		});
	}
}
