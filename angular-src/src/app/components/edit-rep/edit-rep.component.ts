import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-edit-rep',
  templateUrl: './edit-rep.component.html',
  styleUrls: ['./edit-rep.component.css']
})
export class EditRepComponent implements OnInit {
	
	//Variable que guarda la información del representante a editar
	representative: any;
	//Atributos que serán utilizados en la edición
	ci: String;
	name: String;
	lastname: String;
	civil_status: String;
	address: String;
	cellphone: String;
	telephone: String;
	email: String;
	occupation: String;
	company: String;
	avg_income: Number;
	house_type: String;
	house_status: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService,
		private validateService:ValidateService
	) { }

	ngOnInit() {
		this.representative = JSON.parse(localStorage.getItem('data'));
		//Inicialización de los atributos del representante a editar
		this.ci = this.representative.ci;
		this.name = this.representative.name;
		this.lastname = this.representative.lastname;
		this.civil_status = this.representative.civil_status;
		this.address = this.representative.address;
		this.cellphone = this.representative.cellphone;
		this.telephone = this.representative.telephone;
		this.email = this.representative.email;
		this.occupation = this.representative.occupation;
		this.company = this.representative.company;
		this.avg_income = this.representative.avg_income;
		this.house_type = this.representative.house_type;
		this.house_status = this.representative.house_status;
	}
	
	//Editar Representante
	onEditSubmit() {
		const representative = {
			//Atributos
			ci: this.ci,
			name: this.name,
			lastname: this.lastname,
			civil_status: this.civil_status,
			address: this.address,
			cellphone: (this.cellphone == "" ? undefined : this.cellphone),
			telephone: (this.telephone == "" ? undefined : this.telephone),
			email: (this.email == "" ? undefined : this.email),
			occupation: (this.occupation == "" ? undefined : this.occupation),
			company: (this.company == "" ? undefined : this.company),
			avg_income: this.avg_income,
			house_type: this.house_type,
			house_status: this.house_status
		}
		
		//Validaciones
		//Cédula
		if(!this.validateService.validateCI(representative.ci)) {
			this.flashMessage.show('La cédula introducida es inválida', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Números de Teléfono
		if(representative.cellphone != undefined && !this.validateService.validateTlf(representative.cellphone)) {
			this.flashMessage.show('El número de celular es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		if(representative.telephone != undefined && !this.validateService.validateTlf(representative.telephone)) {
			this.flashMessage.show('El número de teléfono (casa) es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Correo Electrónico
		if(representative.email != undefined && !this.validateService.validateEmail(representative.email)) {
			this.flashMessage.show('El correo electrónico es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Campos requeridos
		if(!this.validateService.validateRepresentative(representative)) {
			this.flashMessage.show('Porfavor llene todos los campos obligatorios', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Editar representante despues de realizar las validaciones
		this.authService.editRepresentative(representative).subscribe(data => {
			if(data.success) {
				this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/search']);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
			}
		});
	}
}
