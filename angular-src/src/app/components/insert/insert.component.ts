import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {
	//Atributos que serán utilizados en la inserción
	ci_type: string;
	ci: String;
	name: String;
	lastname: String;
	country: String;
	state: String;
	municipality: String;
	cellphone: String;
	email: String;
	hobby: String;
	birthdate: Date;
	gender: String;
	scholarship: Boolean;
	left_handed: Boolean;
	canaima: Boolean;
	height: Number;
	weight: Number;
	shirt_size: String;
	pants_size: Number;
	shoe_size: Number;
	//Atributos Representante
	rci_type: string;
	r_ci: String;
	kinship: String;

	constructor(
		private validateService: ValidateService,
		private flashMessage: FlashMessagesService,
		private authService: AuthService,
		private router: Router
	) { }

	ngOnInit() {
	}
	
	onInsertSubmit() {
		const student = {
			//Atributos del estudiante
			ci: this.ci_type + this.ci,
			name: this.name,
			lastname: this.lastname,
			country: this.country,
			state: this.state,
			municipality: this.municipality,
			cellphone: (this.cellphone == "" ? undefined : this.cellphone),
			email: (this.email == "" ? undefined : this.email),
			hobby: (this.hobby == "" ? undefined : this.hobby),
			birthdate: this.birthdate,
			gender: this.gender,
			scholarship: ((this.scholarship == undefined) || (!this.scholarship) ? false : true),
			left_handed: ((this.left_handed == undefined) || (!this.left_handed) ? false : true),
			canaima: ((this.canaima == undefined) || (!this.canaima) ? false : true),
			height: this.height,
			weight: this.weight,
			shirt_size: this.shirt_size,
			pants_size: this.pants_size,
			shoe_size: this.shoe_size,
			//Atributos del representante
			representative: {
				r_ci: this.rci_type + this.r_ci,
				kinship: (this.kinship == "" ? undefined : this.kinship)
			}
		}
		
		//Validaciones
		//Cédulas
		if(!this.validateService.validateCI(student.ci)) {
			this.flashMessage.show('La cédula del estudiante es inválida', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		if(!this.validateService.validateCI(student.representative.r_ci)) {
			this.flashMessage.show('La cédula del representante es inválida', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Número de Teléfono
		if(student.cellphone != undefined && !this.validateService.validateTlf(student.cellphone)) {
			this.flashMessage.show('El número de celular del estudiante es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Correo Electrónico
		if(student.email != undefined && !this.validateService.validateEmail(student.email)) {
			this.flashMessage.show('El correo electrónico del estudiante es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Campos requeridos
		if(!this.validateService.validateStudent(student)) {
			this.flashMessage.show('Porfavor llene todos los campos obligatorios', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Validar representante
		this.authService.searchRepresentative(student.representative).subscribe(data => {
			if(data.success) {
				//Insertar estudiante despues de realizar las validaciones
				this.authService.insertStudent(student).subscribe(data => {
					if(data.success) {
						this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
						this.router.navigate(['/']);
					}
					else {
						this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
						this.router.navigate(['/insert']);
					}
				});
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 6000});
				this.flashMessage.show('Es necesario que inserte información de Representante antes de registrar Estudiante', {cssClass: 'alert-danger', timeout: 6000});
			}
		});
	}
}
