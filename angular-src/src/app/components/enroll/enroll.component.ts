import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-enroll',
  templateUrl: './enroll.component.html',
  styleUrls: ['./enroll.component.css']
})
export class EnrollComponent implements OnInit {

	//Variable que guarda información del estudiante a modificar
	student: any;
	//Atributos que serán utilizados en la modificación
	ci: String;
	grade: String;
	section: String;
	commit: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService
	) { }

	ngOnInit() {
		this.student = JSON.parse(localStorage.getItem('data'));
		//Inicialización de los atributos del estudiante a modificar
		this.ci = this.student.ci;
	}
	
	//Modificar estudiante
	onEnrollSubmit() {
		const student = {
			//Atributos del estudiante
			ci: this.ci,
			grade: (this.grade == "" ? undefined : this.grade),
			section: (this.section == "" ? undefined : this.section),
			commit: (this.section == "" ? undefined : this.commit)
		}
		//Validaciones
		//Campos requeridos
		if(student.commit == "Inscripción" || student.commit == "Cambio de Sección" || student.commit == "Cambio de Grado" ) {
			if(student.grade == undefined || student.section == undefined) {
				this.flashMessage.show('Porfavor llene todos los campos', {cssClass: 'alert-danger', timeout: 3000});
				return false;
			}
		} 
		else if(student.commit == "Expulsión" || student.commit == "Graduación") {
			if(student.grade != undefined || student.section != undefined) {
				this.flashMessage.show('Valores Inválidos', {cssClass: 'alert-danger', timeout: 3000});
				return false;
			}
		}
		else {
			this.flashMessage.show('Porfavor llene todos los campos', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		//Realizar modificación del estudiante despues de realizar las validaciones
		this.authService.enrollStudent(student).subscribe(data => {
			if(data.success) {
				this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
				this.authService.deleteStoredData();
				this.router.navigate(['/enrollment']);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
			}
		});
	}
}
