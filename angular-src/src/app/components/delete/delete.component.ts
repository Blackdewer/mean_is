import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

	//Variable que guarda información del estudiante a eliminar para que esta sea mostrada en la página
	student: any;
	//Atributos
	ci: String;
	name: String;
	lastname: String;
	country: String;
	state: String;
	municipality: String;
	cellphone: String;
	email: String;
	hobby: String;
	birthdate: Date;
	gender: String;
	scholarship: String;
	left_handed: String;
	canaima: String;
	height: Number;
	weight: Number;
	shirt_size: String;
	pants_size: Number;
	shoe_size: Number;
	//Atributos Representante
	r_ci: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService,
		private validateService:ValidateService
	) { }

	ngOnInit() {
		this.student = JSON.parse(localStorage.getItem('data'));
		//Inicialización de los atributos del estudiante que son mostrados en pantalla
		this.ci = this.student.ci;
		this.name = this.student.name;
		this.lastname = this.student.lastname;
		this.country = this.student.country;
		this.state = this.student.state;
		this.municipality = this.student.municipality;
		this.cellphone = this.student.cellphone;
		this.email = this.student.email;
		this.hobby = this.student.hobby;
		this.birthdate = this.student.birthdate;
		this.gender = this.student.gender;
		this.scholarship = ((this.student.scholarship == true) ? "Si" : "No");
		this.left_handed = ((this.student.left_handed == true) ? "Si" : "No");
		this.canaima = ((this.student.canaima == true) ? "Si" : "No");
		this.height = this.student.height;
		this.weight = this.student.weight;
		this.shirt_size = this.student.shirt_size;
		this.pants_size = this.student.pants_size;
		this.shoe_size = this.student.shoe_size;
		//Inicialización de los atributos del representante
		this.r_ci = this.student.representative.r_ci;
	}
	//Eliminar estudiante
	onDeleteClick() {
		if(window.confirm('¿Esta usted seguro que desea borrar la información del estudiante?')){
			this.authService.deleteStudent().subscribe(data => {
				if(data.success) {
					this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
					this.authService.deleteStoredData();
					this.router.navigate(['/search']);
				}
				else {
					this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				}
			});
		}
	}
}
