import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertRepComponent } from './insert-rep.component';

describe('InsertRepComponent', () => {
  let component: InsertRepComponent;
  let fixture: ComponentFixture<InsertRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
