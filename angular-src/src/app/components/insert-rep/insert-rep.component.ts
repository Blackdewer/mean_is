import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insert-rep',
  templateUrl: './insert-rep.component.html',
  styleUrls: ['./insert-rep.component.css']
})
export class InsertRepComponent implements OnInit {
	//Atributos que serán utilizados en la inserción
	ci_type: string;
	ci: String;
	name: String;
	lastname: String;
	civil_status: String;
	address: String;
	cellphone: String;
	telephone: String;
	email: String;
	occupation: String;
	company: String;
	avg_income: Number;
	house_type: String;
	house_status: String;

	constructor(
		private validateService: ValidateService,
		private flashMessage: FlashMessagesService,
		private authService: AuthService,
		private router: Router
	) { }

	ngOnInit() {
	}
	
	onInsertSubmit() {
		const representative = {
			//Atributos
			ci: this.ci_type + this.ci,
			name: this.name,
			lastname: this.lastname,
			civil_status: this.civil_status,
			address: this.address,
			cellphone: (this.cellphone == "" ? undefined : this.cellphone),
			telephone: (this.telephone == "" ? undefined : this.telephone),
			email: (this.email == "" ? undefined : this.email),
			occupation: (this.occupation == "" ? undefined : this.occupation),
			company: (this.company == "" ? undefined : this.company),
			avg_income: this.avg_income,
			house_type: this.house_type,
			house_status: this.house_status
		}
		
		//Validaciones
		//Cédula
		if(!this.validateService.validateCI(representative.ci)) {
			this.flashMessage.show('La cédula introducida es inválida', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Números de Teléfono
		if(representative.cellphone != undefined && !this.validateService.validateTlf(representative.cellphone)) {
			this.flashMessage.show('El número de celular es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		if(representative.telephone != undefined && !this.validateService.validateTlf(representative.telephone)) {
			this.flashMessage.show('El número de teléfono (casa) es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Correo Electrónico
		if(representative.email != undefined && !this.validateService.validateEmail(representative.email)) {
			this.flashMessage.show('El correo electrónico es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Campos requeridos
		if(!this.validateService.validateRepresentative(representative)) {
			this.flashMessage.show('Porfavor llene todos los campos obligatorios', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Insertar representante despues de realizar las validaciones
		this.authService.insertRepresentative(representative).subscribe(data => {
			if(data.success) {
				this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/']);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				this.router.navigate(['/insert-rep']);
			}
		});
	}
}
