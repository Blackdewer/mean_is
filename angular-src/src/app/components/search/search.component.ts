import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
	
	ci_type: string;
	ci: String;
	rci_type: string;
	r_ci: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService
	) { }

	ngOnInit() {
	}
	
	//Buscar información de estudiante que será editado
	studentEdit() {
		const student = { 
			ci: this.ci_type + this.ci
		}
		this.authService.searchStudent(student).subscribe(data => {
			if(data.success) {
				this.authService.storeData(data.student);
				this.flashMessage.show('Información de estudiante encontrada', {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/edit/'+data.student._id]);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				this.router.navigate(['/search']);
			}
		});
	}
	//Buscar información del representante que será editado
	repEdit() {
		const representative = { 
			r_ci: this.rci_type + this.r_ci
		}
		this.authService.searchRepresentative(representative).subscribe(data => {
			if(data.success) {
				this.authService.storeData(data.representative);
				this.flashMessage.show('Información de representante encontrada', {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/edit-rep/'+data.representative._id]);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				this.router.navigate(['/search']);
			}
		});
	}
	//Buscar información de estudiante que será eliminado
	studentDelete() {
		const student = { 
			ci: this.ci_type + this.ci
		}
		this.authService.searchStudent(student).subscribe(data => {
			if(data.success) {
				this.authService.storeData(data.student);
				this.flashMessage.show('Información de estudiante encontrada', {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/delete/'+data.student._id]);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				this.router.navigate(['/search']);
			}
		});
	}
	//Buscar información de representante que será eliminado
	repDelete() {
		const representative = { 
			r_ci: this.rci_type + this.r_ci
		}
		this.authService.searchRepresentative(representative).subscribe(data => {
			if(data.success) {
				this.authService.storeData(data.representative);
				this.flashMessage.show('Información de representante encontrada', {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/delete-rep/'+data.representative._id]);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				this.router.navigate(['/search']);
			}
		});
	}
}
