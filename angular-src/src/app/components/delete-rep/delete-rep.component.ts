import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-delete-rep',
  templateUrl: './delete-rep.component.html',
  styleUrls: ['./delete-rep.component.css']
})
export class DeleteRepComponent implements OnInit {

	//Variable que guarda la información del representante
	representative: any;
	//Atributos utilizados
	ci: String;
	name: String;
	lastname: String;
	civil_status: String;
	address: String;
	cellphone: String;
	telephone: String;
	email: String;
	occupation: String;
	company: String;
	avg_income: Number;
	house_type: String;
	house_status: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService,
		private validateService:ValidateService
	) { }

	ngOnInit() {
		this.representative = JSON.parse(localStorage.getItem('data'));
		//Inicialización de los atributos del representante
		this.ci = this.representative.ci;
		this.name = this.representative.name;
		this.lastname = this.representative.lastname;
		this.civil_status = this.representative.civil_status;
		this.address = this.representative.address;
		this.cellphone = this.representative.cellphone;
		this.telephone = this.representative.telephone;
		this.email = this.representative.email;
		this.occupation = this.representative.occupation;
		this.company = this.representative.company;
		this.avg_income = this.representative.avg_income;
		this.house_type = this.representative.house_type;
		this.house_status = this.representative.house_status;
	}
 
	//Eliminar representante
	onDeleteClick() {
		const representative = this.representative;
		if(window.confirm('¿Esta usted seguro que desea borrar la información del representante?')){
			this.authService.deleteRepresentative(representative).subscribe(data => {
				if(data.success) {
					this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
					this.authService.deleteStoredData();
					this.router.navigate(['/search']);
				}
				else {
					this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 6000});
				}
			});
		}
	}
}
