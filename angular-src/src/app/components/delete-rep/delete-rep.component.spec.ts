import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteRepComponent } from './delete-rep.component';

describe('DeleteRepComponent', () => {
  let component: DeleteRepComponent;
  let fixture: ComponentFixture<DeleteRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
