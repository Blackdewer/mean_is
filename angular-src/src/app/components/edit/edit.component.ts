import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
	
	//Variable que guarda la información del estudiante a editar
	student: any;
	//Atributos que serán utilizados en la edición
	ci: String;
	name: String;
	lastname: String;
	country: String;
	state: String;
	municipality: String;
	cellphone: String;
	email: String;
	hobby: String;
	birthdate: Date;
	gender: String;
	scholarship: Boolean;
	left_handed: Boolean;
	canaima: Boolean;
	height: Number;
	weight: Number;
	shirt_size: String;
	pants_size: Number;
	shoe_size: Number;
	//Atributos Representante
	rci_type: string;
	r_ci: String;
	kinship: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService,
		private validateService:ValidateService
	) { }

	ngOnInit() {
		this.student = JSON.parse(localStorage.getItem('data'));
		//Inicialización de los atributos del estudiante a editar
		this.ci = this.student.ci;
		this.name = this.student.name;
		this.lastname = this.student.lastname;
		this.country = this.student.country;
		this.state = this.student.state;
		this.municipality = this.student.municipality;
		this.cellphone = this.student.cellphone;
		this.email = this.student.email;
		this.hobby = this.student.hobby;
		this.birthdate = this.student.birthdate;
		this.gender = this.student.gender;
		this.scholarship = this.student.scholarship;
		this.left_handed = this.student.left_handed;
		this.canaima = this.student.canaima;
		this.height = this.student.height;
		this.weight = this.student.weight;
		this.shirt_size = this.student.shirt_size;
		this.pants_size = this.student.pants_size;
		this.shoe_size = this.student.shoe_size;
		//Inicialización de los atributos del representante
		this.rci_type = this.student.representative.r_ci.substring(0,1);
		this.r_ci = this.student.representative.r_ci.substring(1,this.student.representative.r_ci.length);
		this.kinship = this.student.representative.kinship;
	}
	//Editar estudiante
	onEditSubmit() {
		const student = {
			//Atributos del estudiante
			ci: this.ci,
			name: this.name,
			lastname: this.lastname,
			country: this.country,
			state: this.state,
			municipality: this.municipality,
			cellphone: (this.cellphone == "" ? undefined : this.cellphone),
			email: (this.email == "" ? undefined : this.email),
			hobby: (this.hobby == "" ? undefined : this.hobby),
			birthdate: this.birthdate,
			gender: this.gender,
			scholarship: ((this.scholarship == undefined) || (!this.scholarship) ? false : true),
			left_handed: ((this.left_handed == undefined) || (!this.left_handed) ? false : true),
			canaima: ((this.canaima == undefined) || (!this.canaima) ? false : true),
			height: this.height,
			weight: this.weight,
			shirt_size: this.shirt_size,
			pants_size: this.pants_size,
			shoe_size: this.shoe_size,
			//Atributos del representante
			representative: {
				r_ci: this.rci_type + this.r_ci,
				kinship: (this.kinship == "" ? undefined : this.kinship)
			}
		}
		
		//Validaciones
		//Cédula
		if(!this.validateService.validateCI(student.representative.r_ci)) {
			this.flashMessage.show('La cédula del representante es inválida', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Número de Teléfono
		if(student.cellphone != undefined && !this.validateService.validateTlf(student.cellphone)) {
			this.flashMessage.show('El número de celular del estudiante es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Correo Electrónico
		if(student.email != undefined && !this.validateService.validateEmail(student.email)) {
			this.flashMessage.show('El correo electrónico del estudiante es inválido', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}
		
		//Campos requeridos
		if(!this.validateService.validateStudent(student)) {
			this.flashMessage.show('Porfavor llene todos los campos obligatorios', {cssClass: 'alert-danger', timeout: 3000});
			return false;
		}

		//Validar representante
		this.authService.searchRepresentative(student.representative).subscribe(data => {
			if(data.success) {
				//Editar información de estudiante despues de realizar las validaciones
				this.authService.editStudent(student).subscribe(data => {
					if(data.success) {
						this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
						this.authService.deleteStoredData();
						this.router.navigate(['/search']);
					}
					else {
						this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
					}
				});
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 6000});
				this.flashMessage.show('Es necesario una cédula válida de representate a la hora de editar el estudiante', {cssClass: 'alert-danger', timeout: 6000});
			}
		});
	}
}
