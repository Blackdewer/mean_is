import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.css']
})
export class EnrollmentComponent implements OnInit {

	ci_type: string;
	ci: String;

	constructor(
		private authService:AuthService,
		private router:Router,
		private flashMessage:FlashMessagesService
	) { }

	ngOnInit() {
	}

	//Buscar información de estudiante para futura inscripción/modificación
	studentEnroll() {
		const student = { 
			ci: this.ci_type + this.ci
		}
		this.authService.searchStudent(student).subscribe(data => {
			if(data.success) {
				this.authService.storeData(data.student);
				this.flashMessage.show('Información de estudiante encontrada', {cssClass: 'alert-success', timeout: 3000});
				this.router.navigate(['/enroll/'+data.student._id]);
			}
			else {
				this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
				this.router.navigate(['/enrollment']);
			}
		});
	}
}
