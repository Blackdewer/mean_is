//Servicio utilizado para realizar validaciones dentro de los campos de la aplicación
import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

	constructor() { }
  
	validateRegister(user) {
		if(user.username == undefined || user.password == undefined) {
			return false;
		}
		else {
			return true;
		}
	}
	
	validateStudent(student) {
		if(
			student.ci == undefined || student.name == undefined || student.lastname == undefined ||
			student.country == undefined || student.state == undefined || student.municipality == undefined ||
			student.birthdate == undefined || student.gender == undefined || student.representative.r_ci == undefined
		) {
			return false;
		}
		else {
			return true;
		}
	}
	
	validateRepresentative(representative) {
		if(
			representative.ci == undefined || representative.name == undefined ||
			representative.lastname == undefined || representative.address == undefined
		) {
			return false;
		}
		else {
			return true;
		}
	}
	
	validateCI(ci) {
		const re = /(V|E)\d{7,8}/;
		return re.test(String(ci));
	}
	
	validateTlf(number) {
		const re = /^0((2\d{2}-\d{7})|(4(1[246]|2[46])-\d{7}))/;
		return re.test(String(number));
	}
	
	validateEmail(email) {
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
}
