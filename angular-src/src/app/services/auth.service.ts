import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { HttpModule } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
	
	constructor(private http:Http) { }
	
	//Conexiones con el back-end
	
	//Registro de usuario
	registerUser(user) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/users/register', user, {headers: headers}).map(res => res.json());
	}
	
	//Autenticación de usuario
	authenticateUser(user) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/users/authenticate', user, {headers: headers}).map(res => res.json());
	}
	
	//Almacenar información de token y usuario luego de autenticar para su posterior uso
	storeUserData(token, user) {
		localStorage.setItem('id_token', token);
		localStorage.setItem('user', JSON.stringify(user));
	}
	
	adminLoggedIn() {
		if((tokenNotExpired('id_token')) && (JSON.parse(localStorage.getItem('user')).role == 'Administrador')) {
			return true;
		}
		else {
			return false;
		}
	}
	
	loggedIn() {
		return tokenNotExpired('id_token');
	}
	
	//Logout
	logout() {
		localStorage.clear();
	}
	
	//Inserción de Representante
	insertRepresentative(representative) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/representatives/insert', representative, {headers: headers}).map(res => res.json());
	}
	
	//Búsqueda de representante para futura edición o eliminación
	searchRepresentative(representative) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/representatives/search', representative, {headers: headers}).map(res => res.json());
	}
	
	//Editar información de representante
	editRepresentative(representative) {
		let headers = new Headers();
		let data = {representative: representative, user: JSON.parse(localStorage.getItem('user'))}
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/representatives/edit-rep/'+JSON.parse(localStorage.getItem('data'))._id, data, {headers: headers}).map(res => res.json());
	}

	//Eliminar información de representante
	deleteRepresentative(representative) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/representatives/delete-rep/'+JSON.parse(localStorage.getItem('data'))._id, representative, {headers: headers}).map(res => res.json());
	}
	
	//Inserción de Estudiante
	insertStudent(student) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/students/insert', student, {headers: headers}).map(res => res.json());
	}

	//Editar información de estudiante
	editStudent(student) {
		let headers = new Headers();
		let data = {student: student, user: JSON.parse(localStorage.getItem('user'))};
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/students/edit/'+JSON.parse(localStorage.getItem('data'))._id, data, {headers: headers}).map(res => res.json());
	}
		
	//Eliminar información de estudiante
	deleteStudent() {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.delete('http://localhost:3000/students/delete/'+JSON.parse(localStorage.getItem('data'))._id, {headers: headers}).map(res => res.json());
	}

	//Realizar modificacion de estudiante
	enrollStudent(student) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/students/enroll/'+JSON.parse(localStorage.getItem('data'))._id, student, {headers: headers}).map(res => res.json());
	}
	
	//Búsqueda de estudiante para futura edición o eliminación
	searchStudent(student) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post('http://localhost:3000/students/search', student, {headers: headers}).map(res => res.json());
	}
	
	//Almacenar información auxiliar localmente para su posterior uso
	storeData(data) {
		localStorage.setItem('data', JSON.stringify(data));
	}
	
	//Eliminar información almacenada localmente
	deleteStoredData() {
		localStorage.removeItem('data');
	}
	
	//Exportación de datos
	exportData1() {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.get('http://localhost:3000/students/export1', {headers: headers}).map(res => res.json());
	}
	
}
