import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { CsvService } from 'angular2-json2csv';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { InsertComponent } from './components/insert/insert.component';
import { InsertRepComponent } from './components/insert-rep/insert-rep.component';
import { SearchComponent } from './components/search/search.component';
import { EditComponent } from './components/edit/edit.component';
import { EditRepComponent } from './components/edit-rep/edit-rep.component';
import { DeleteComponent } from './components/delete/delete.component';
import { DeleteRepComponent } from './components/delete-rep/delete-rep.component';
import { EnrollmentComponent } from './components/enrollment/enrollment.component';
import { EnrollComponent } from './components/enroll/enroll.component';
import { ExportComponent } from './components/export/export.component';

import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

//Variable que contiene información de las rutas que conforman la aplicación
const appRoutes: Routes = [
	{path:'', component: HomeComponent},
	{path:'register', component: RegisterComponent, canActivate:[AdminGuard]},
	{path:'login', component: LoginComponent},
	{path:'search', component: SearchComponent, canActivate:[AdminGuard]},
	{path:'insert', component: InsertComponent, canActivate:[AuthGuard]},
	{path:'insert-rep', component: InsertRepComponent, canActivate:[AuthGuard]},
	{path:'edit/:id', component: EditComponent, canActivate:[AdminGuard]},
	{path:'edit-rep/:id', component: EditRepComponent, canActivate:[AdminGuard]},
	{path:'delete/:id', component: DeleteComponent, canActivate:[AdminGuard]},
	{path:'delete-rep/:id', component: DeleteRepComponent, canActivate:[AdminGuard]},
	{path:'enrollment', component: EnrollmentComponent, canActivate:[AuthGuard]},
	{path:'enroll/:id', component: EnrollComponent, canActivate:[AuthGuard]},
	{path:'export', component: ExportComponent, canActivate:[AuthGuard]}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
	SearchComponent,
    InsertComponent,
	InsertRepComponent,
    EditComponent,
	EditRepComponent,
    DeleteComponent,
    DeleteRepComponent,
    EnrollmentComponent,
    EnrollComponent,
    ExportComponent
  ],
  imports: [
    BrowserModule,
	RouterModule.forRoot(appRoutes),
	FormsModule,
	HttpModule,
	FlashMessagesModule.forRoot()
  ],
  providers: [ValidateService, AuthService, AuthGuard, AdminGuard, CsvService],
  bootstrap: [AppComponent]
})
export class AppModule { }
