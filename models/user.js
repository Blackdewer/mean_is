//Dependencias
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const config = require('../config/database');

//User Schema
const UserSchema = mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	role: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	}
});

//Exportación del Schema "User"
const User = module.exports = mongoose.model('User', UserSchema);

//Funciones adicionales

//Función de registro de usuario
module.exports.addUser = function(newUser, callback) {
	bcrypt.genSalt(10, function(err, salt) {
		bcrypt.hash(newUser.password, salt, function(err, hash) {
			if(err) throw err;
			newUser.password = hash;
			newUser.save(callback);		
		});
	});
}

//Buscar usuario a partir del ID
module.exports.getUserById = function(id, callback) {
	User.findById(id, callback);
}

//Buscar usuario por el nombre
module.exports.getUserByUsername = function(username, callback) {
	const query = {username:username}
	User.findOne(query, callback);
}

//Comparación de contraseñas
module.exports.comparePassword = function(candidatePassword, hash, callback) {
	bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
		if(err) throw err;
		callback(null, isMatch);
	});
}