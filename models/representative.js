//Dependencias
const mongoose = require('mongoose');
const config = require('../config/database');

//Representative Schema
const RepresentativeSchema = mongoose.Schema({
	ci: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	lastname: {
		type: String,
		required: true
	},
	civil_status: {
		type: String
	},
	address: {
		type: String,
		required: true
	},
	cellphone: {
		type: String
	},
	telephone: {
		type: String
	},
	email: {
		type: String
	},
	occupation: {
		type: String
	},
	company: {
		type:String
	},
	avg_income: {
		type: Number
	},
	house_type: {
		type: String
	},
	house_status: {
		type: String
	},
	last_modified: {
		date: {
			type: Date
		},
		by_user: {
			type: String
		}
	}
});

//Exportación del Schema "Representative"
const Representative = module.exports = mongoose.model('Representative', RepresentativeSchema);

//Funciones adicionales

//Función de registro de representante
module.exports.addRepresentative = function(newRepresentative, callback) {
	newRepresentative.save(callback);
}

//Buscar representante a partir del ID
module.exports.getRepresentative = function(id, callback) {
	Representative.findById(id, callback);
}

//Buscar representante por cédula
module.exports.getRepresentativeByCI = function(ci, callback) {
	const query = {ci:ci}
	Representative.findOne(query, callback);
}