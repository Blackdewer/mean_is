//Dependencias
const mongoose = require('mongoose');
const config = require('../config/database');

//Student Schema
const StudentSchema = mongoose.Schema({
	ci: {
		type: String,
		required: true
	},
	grade: {
		type: String
	},
	section: {
		type: String
	},
	name: {
		type: String,
		required: true
	},
	lastname: {
		type: String,
		required: true
	},
	country: {
		type: String,
		required: true
	},
	state: {
		type: String,
		required: true
	},
	municipality: {
		type: String,
		required: true
	},
	cellphone: {
		type: String
	},
	email: {
		type: String
	},
	hobby: {
		type: String
	},
	birthdate: {
		type: String,
		required: true
	},
	gender: {
		type: String,
		required: true
	},
	scholarship: {
		type: Boolean
	},
	left_handed: {
		type: Boolean
	},
	canaima: {
		type: Boolean
	},
	height: {
		type: Number
	},
	weight: {
		type: Number
	},
	shirt_size: {
		type: String
	},
	pants_size: {
		type: Number
	},
	shoe_size: {
		type: Number
	},
	representative: {
		r_ci: {
			type: String
		},
		kinship: {
			type: String
		}
	},
	last_modified: {
		date: {
			type: Date
		},
		by_user: {
			type: String
		}
	},
	enroll_log: [
		{
			grade: {
				type: String
			},
			section: {
				type: String
			},
			commit: {
				type: String
			},
			date: {
				type: Date
			}
		}
	]
});

//Exportación del Schema "Student"
const Student = module.exports = mongoose.model('Student', StudentSchema);

//Funciones adicionales

//Función de registro de estudiante
module.exports.addStudent = function(newStudent, callback) {
	newStudent.save(callback);
}

//Buscar estudiante a partir del ID
module.exports.getStudentById = function(id, callback) {
	Student.findById(id, callback);
}

//Buscar estudiante por cédula
module.exports.getStudentByCI = function(ci, callback) {
	const query = {ci:ci}
	Student.findOne(query, callback);
}

//Buscar estudiantes por cédula de representante
module.exports.getStudentByRCI = function(r_ci, callback) {
	const query = {"representative.r_ci" : r_ci}
	Student.find(query, callback);
}