# mean_is

                Sistema de gestión de inscripción para el Liceo Bolivariano
                                Caracciolo Parra y Olmedo

***********************************************************************************************

     v1.1 (20/06/2018)
    -Separacion de estudiante y representante (Insercion, modificiacion y borrado).
    -Separacion de inscripcion de estudiantes en el proceso de insercion.
    -Algunas mejoras funcionales.

    v1.2 (20/06/2018)
    -Creacion de Exportacion para archivos .csv
    -Creacion de Botones para las nuevas funcionalidades
    -Creacion de Manual de Usuario

    NOTA: para poder correr el proyecto se deben tener los frameworks:
    MongoDB
    Express
    Angular
    Node.js